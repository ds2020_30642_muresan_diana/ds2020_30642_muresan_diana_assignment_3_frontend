import React from 'react';
import validate from "./validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class PatientUpdateForm extends React.Component {


    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.reloadHandler = this.props.reloadHandler;
        this.patient = this.props.patient;
        this.caregiverName = null;

        console.log(this.patient);


        this.state = {


            errorStatus: 0,
            error: null,

            formIsValid: true,

            formControls: {
                id: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },
                name: {
                    value: '',
                    placeholder:  '' ,
                    valid: false,
                    touched: false

                },
                birthdate: {
                    value:  '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },
                gender: {
                    value:  '',
                    placeholder:  '',
                    valid: false,
                    touched: false

                },
                address: {
                    value:   '',
                    placeholder:  '',
                    valid: false,
                    touched: false,
                },
                medicalRecord: {
                    value:  '',
                    placeholder:  '',
                    valid: false,
                    touched: false,
                },
                username: {
                    value:  '',
                    placeholder:  '',
                    valid: false,
                    touched: false,
                },
                password: {
                    value:  '',
                    placeholder:  '',
                    valid: false,
                    touched: false,
                },
                caregiverName: {
                    value:  '',
                    placeholder:  '',
                    valid: false,
                    touched: false,
                },


            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;

        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid

        }

        this.setState({
            formControls: updatedControls,
            formIsValid: true
        });
    };



    updatePatient(patient, caregiverName) {
        return API_USERS.updatePatient(patient, caregiverName, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }



    handleEdit() {
        console.log(this.patient.name);
        let name, birthdate, gender, address, medicalRecord, username, password, caregiverName = null;
        if (this.state.formControls.name.value === '' )
            name = this.patient.name;
        else name = this.state.formControls.name.value;
        if (this.state.formControls.birthdate.value === '' )
            birthdate = this.patient.birthdate;
        else birthdate = this.state.formControls.birthdate.value;
        if (this.state.formControls.gender.value === '' )
            gender = this.patient.gender;
        else gender = this.state.formControls.gender.value;
        if (this.state.formControls.address.value === '' )
            address = this.patient.address;
        else address = this.state.formControls.address.value;
        if (this.state.formControls.medicalRecord.value === '' )
            medicalRecord = this.patient.medicalRecord;
        else medicalRecord = this.state.formControls.medicalRecord.value;
        if (this.state.formControls.username.value === '' )
            username = this.patient.username;
        else username = this.state.formControls.username.value;
        if (this.state.formControls.password.value === '' )
            password = this.patient.password;
        else password = this.state.formControls.password.value;
        if (this.state.formControls.caregiverName.value === '' )
            this.caregiverName = 'notChanged';
        else this.caregiverName = this.state.formControls.caregiverName.value;


        let patient = {
            id: this.patient.id,
            name: name,
            birthdate: birthdate,
            gender: gender,
            address: address,
            medicalRecord: medicalRecord,
            username: username,
            password: password,
            medicationPlans: this.patient.medicationPlans,

        };



        console.log(patient);
        this.updatePatient(patient, this.caregiverName);

    }

    render() {

        return (
            <div>



                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.patient.name}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}

                          // valid={this.state.formControls.name.valid}

                    />

                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label for='birthdateField'> Birthdate: </Label>
                    <Input name='birthdate' id='birthdateField' placeholder={this.patient.birthdate}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.birthdate.value}
                           touched={this.state.formControls.birthdate.touched? 1 : 0}
                           //valid={this.state.formControls.birthdate.valid}

                    />
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.patient.gender}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           //valid={this.state.formControls.gender.valid}

                    />

                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.patient.address}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           //valid={this.state.formControls.address.valid}

                    />
                </FormGroup>

                <FormGroup id='medicalRecord'>
                    <Label for='medicalRecordField'> Medical record: </Label>
                    <Input name='medicalRecord' id='medicalRecordField' placeholder={this.patient.medicalRecord}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicalRecord.value}
                           touched={this.state.formControls.medicalRecord.touched? 1 : 0}
                           //valid={this.state.formControls.medicalRecord.valid}

                    />
                </FormGroup>

                <FormGroup id='username'>
                    <Label for='usernameField'> Username: </Label>
                    <Input name='username' id='usernameField' placeholder={this.patient.username}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.username.value}
                           touched={this.state.formControls.username.touched? 1 : 0}
                        //valid={this.state.formControls.medicalRecord.valid}

                    />
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.patient.password}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched? 1 : 0}
                        //valid={this.state.formControls.medicalRecord.valid}

                    />
                </FormGroup>

                <FormGroup id='caregiverName'>
                    <Label for='caregiverNameField'> Caregiver name: </Label>
                    <Input name='caregiverName' id='caregiverNameField' placeholder={''}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.caregiverName.touched? 1 : 0}
                        //valid={this.state.formControls.medicalRecord.valid}

                    />
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleEdit}> Edit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>

                }


            </div>
        );

    }


}

export default PatientUpdateForm;