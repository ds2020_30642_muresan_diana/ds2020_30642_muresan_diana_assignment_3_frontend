import React from 'react';
import validate from "./validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS_2 from "../../medication-plan/api/medication-plan-api"
import * as API_USERS from "../../medication/api/medication-api"
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import MedicationTableMedplan from "./medication-table-medplan";
import moment from "moment";


class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.patientId = this.props.patientId;
        console.log("id patient bun" + this.patientId);
        this.medicationPlanId = null;
        this.medication = {
            id: null,
            name: null,
            sideEffects: null,
            dosage: null
        }
        this.medicationFinalList = [];
        this.medicationId = null;
        this.medicationIdList = [];
        this.result2 = [];

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            isLoaded: false,
            isLoaded1: false,

            formControls: {
                intakeIntervals: {
                    value: '',
                    placeholder: 'Intake Intervals...',
                    valid: false,
                    touched: false,
                },
                periodOfTreatment: {
                    value: '',
                    placeholder: 'Period of Treatment...',
                    valid: false,
                    touched: false
                },



            }
        };


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    componentDidMount() {
        this.fetchMedications();

    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;

        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid

        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    getMedicationById(id) {
        return API_USERS.getMedicationById(id, (result, status, err) => {
            if (result !== null && status === 200) {
                this.medication.id = result.id;
                this.medication.name = result.name;
                this.medication.sideEffects = result.sideEffects;
                this.medication.dosage = result.dosage;
                let me = {
                    id: this.medication.id,
                    name: this.medication.name,
                    sideEffects: this.medication.sideEffects,
                    dosage: this.medication.dosage,
                }
                this.medicationFinalList.push(me);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }))
            }
        })
    }

    fetchMedications() {
        return API_USERS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }





    handleSubmit() {

        API_USERS.getMedicationById(this.medicationIdList[0], (result, status, err) => {
            if (result !== null && status === 200) {
                this.medication.id = result.id;
                this.medication.name = result.name;
                this.medication.sideEffects = result.sideEffects;
                this.medication.dosage = result.dosage;

                let medicationPlan = {
                    intakeIntervals: this.state.formControls.intakeIntervals.value,
                    periodOfTreatment: this.state.formControls.periodOfTreatment.value,
                    medications: [this.medication],
                    idPatient: this.patientId
                };
                console.log(this.medication);

                API_USERS_2.postMedicationPlan(medicationPlan, this.patientId, (result2, status, error) => {
                    if (result2 !== null && (status === 200 || status === 201)) {
                        console.log("Successfully inserted medication plan with id: " + result2);
                        // this.reloadHandler();
                        this.medicationPlanId = result2;

                        for (let i = 1 ; i < this.medicationIdList.length ; i++) {
                            let med = {
                                id:null,
                                name: null,
                                sideEffects:null,
                                dosage: null,
                                idPatient: this.patientId,
                                idMedicationPlan: this.medicationPlanId
                            }
                            API_USERS.getMedicationById(this.medicationIdList[i], (result, status, err) => {
                                if (result !== null && status === 200) {
                                    med.id = result.id;
                                    med.name = result.name;
                                    med.sideEffects = result.sideEffects;
                                    med.dosage = result.dosage;
                                    console.log(med);
                                    console.log("medication id" + this.medicationPlanId);
                                    console.log("patient id" + this.patientId);

                                    API_USERS_2.updateMedicationPlan(med, this.medicationPlanId, this.patientId, (result, status, error) => {
                                        if (result !== null && (status === 200 || status === 201)) {
                                            console.log("Successfully updated medication plan with id: " + result);
                                            // this.reloadHandler();
                                            this.medicationPlanId = result;
                                        } else {
                                            this.setState(({
                                                errorStatus: status,
                                                error: error
                                            }));
                                        }
                                    });

                                } else {
                                    this.setState(({
                                        errorStatus: status,
                                        error: err
                                    }))
                                }
                            });



                        }
                    } else {
                        this.setState(({
                            errorStatus: status,
                            error: error
                        }));
                    }
                });




            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }))
            }
        });



        this.reloadHandler();
    }

    handleSelect = (medicationId) => {
        console.log(medicationId);
        this.medicationId = medicationId;
        this.medicationIdList.push(medicationId);


    }


    render() {
        return (
            <div>

                <FormGroup id='intakeIntervals'>
                    <Label for='intakeIntervalsField'> Intake intervals: </Label>
                    <Input name='intakeIntervals' id='intakeIntervalsField' placeholder={this.state.formControls.intakeIntervals.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.intakeIntervals.value}
                           touched={this.state.formControls.intakeIntervals.touched? 1 : 0}
                           valid={this.state.formControls.intakeIntervals.valid}
                           required
                    />

                </FormGroup>

                <FormGroup id='periodOfTreatment'>
                    <Label for='periodOfTreatmentField'> Period of Treatment: </Label>
                    <Input name='periodOfTreatment' id='periodOfTreatmentField' placeholder={this.state.formControls.periodOfTreatment.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.periodOfTreatment.value}
                           touched={this.state.formControls.periodOfTreatment.touched? 1 : 0}
                           valid={this.state.formControls.periodOfTreatment.valid}
                           required
                    />
                </FormGroup>



                <Row className={"table-row"}>
                    <Col sm={{size: '12', offset: 0}}>

                        {this.state.isLoaded && <MedicationTableMedplan tableData = {this.state.tableData} reloadHandler={this.reload} medicationId={this.handleSelect}  />}
                        {this.state.errorStatus > 0 && <APIResponseErrorMessage
                            errorStatus={this.state.errorStatus}
                            error={this.state.error}
                        />   }
                    </Col>
                </Row>



                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>

                }


            </div>
        );

    }


}

export default MedicationPlanForm;