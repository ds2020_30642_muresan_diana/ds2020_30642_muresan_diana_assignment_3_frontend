import React from 'react';
import validate from "./validators/medication-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class MedicationUpdateForm extends React.Component {


    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.reloadHandler = this.props.reloadHandler;
        this.medication = this.props.medication;


        this.state = {


            errorStatus: 0,
            error: null,

            formIsValid: true,

            formControls: {
                id: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },
                name: {
                    value: '',
                    placeholder:  '' ,
                    valid: false,
                    touched: false

                },
                sideEffects: {
                    value:  '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },
                dosage: {
                    value:  '',
                    placeholder:  '',
                    valid: false,
                    touched: false

                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;

        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid

        }

        this.setState({
            formControls: updatedControls,
            formIsValid: true
        });
    };



    updateMedication(medication) {
        return API_USERS.updateMedication(medication, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleEdit() {

        let name, sideEffects, dosage = null;
        if (this.state.formControls.name.value === '' )
            name = this.medication.name;
        else name = this.state.formControls.name.value;
        if (this.state.formControls.sideEffects.value === '' )
            sideEffects = this.medication.sideEffects;
        else sideEffects = this.state.formControls.sideEffects.value;
        if (this.state.formControls.dosage.value === '' )
            dosage = this.medication.dosage;
        else dosage = this.state.formControls.dosage.value;



        let medication = {
            id: this.medication.id,
            name: name,
            sideEffects: sideEffects,
            dosage: dosage,

        };

        console.log(medication);
        this.updateMedication(medication);
    }

    render() {

        return (
            <div>



                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.medication.name}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}

                        // valid={this.state.formControls.name.valid}

                    />

                </FormGroup>

                <FormGroup id='sideEffects'>
                    <Label for='sideEffectsField'> Side effects: </Label>
                    <Input name='sideEffects' id='sideEffectsField' placeholder={this.medication.sideEffects}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.sideEffects.value}
                           touched={this.state.formControls.sideEffects.touched? 1 : 0}
                        //valid={this.state.formControls.birthdate.valid}

                    />
                </FormGroup>

                <FormGroup id='dosage'>
                    <Label for='dosageField'> Dosage: </Label>
                    <Input name='dosage' id='dosageField' placeholder={this.medication.dosage}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.dosage.value}
                           touched={this.state.formControls.dosage.touched? 1 : 0}
                        //valid={this.state.formControls.gender.valid}

                    />

                </FormGroup>



                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleEdit}> Edit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>

                }


            </div>
        );

    }


}

export default MedicationUpdateForm;