import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "./api/caregiver-api"
import * as API_USERS_2 from "../patient/api/patient-api"
import CaregiverTable from "./components/caregiver-table";
import CaregiverForm from "./components/caregiver-form";
import moment from "moment";

import CaregiverUpdateForm from "./components/caregiver-update-form";
import PatientTable from "../patient/components/patient-table";
import {Redirect} from "react-router-dom";



class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.reload1 = this.reload1.bind(this);
        this.caregiver =   {
            id: null,
            name: null,
            birthdate: null,
            gender: null,
            address: null,
            username: null,
            password: null

        };
        this.state = {
            showPatients: false,
            updateForm: false,
            selected: false,
            collapseForm: false,
            tableData: [],
            patientData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            params: []
        };
    }

    componentDidMount() {
        this.fetchCaregivers();


    }

    fetchCaregivers() {
        return API_USERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });



                console.log(result);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    getCaregiver(id) {
        return API_USERS.getCaregiverById(id, (result, status, err) => {
            if (result !== null && status === 200) {
                let formatBirthdate = moment(result.birthdate).format('DD/MM/YYYY');
                this.caregiver.id = result.id;
                this.caregiver.name = result.name;
                this.caregiver.birthdate = formatBirthdate;
                this.caregiver.gender = result.gender;
                this.caregiver.address = result.address;
                this.caregiver.username = result.username;
                this.caregiver.password = result.password;

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }))
            }
        })
    }

    getPatientsByCaregiverId(params) {
        return API_USERS_2.getPatientsByCaregiverId(params[0],(result, status, err) => {

            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                     patientData: result,
                     isLoaded: true,
                     showPatients: true
                });


                console.log(params[0]);


            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCaregivers();

    }

    reload1() {
        this.setState({
            isLoaded: false
        });

        this.state.updateForm = false;

        this.fetchCaregivers();

    }

    handleId  = (params) => {
        this.state.params[0] = params[0];

        this.getCaregiver(params[0]);

        if (params[1] === 1) {
            this.state.updateForm = true;
            this.state.showPatients = false;
        }
        else {
            this.state.showPatients = true;
            return API_USERS_2.getPatientsByCaregiverId(params[0],(result, status, err) => {

                if (result !== null && status === 200) {
                    console.log(result);
                    this.setState({
                        patientData: result,
                        isLoaded: true,
                        showPatients: true
                    });

                    this.state.params[0] = params[0];

                    if (params[1] === 1) {
                        this.state.updateForm = true;
                        this.state.showPatients = false;
                    }
                    else {
                        this.state.showPatients = true;

                    }
                   this.toggleForm();

                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                }
            });

        }
        this.toggleForm();


        console.log(params);


    }

    render() {
        const { patientData } = this.state;
        if (localStorage.getItem('user') === "doctor")
            return (
            <div>
                <CardHeader>
                    <span className={"patient-title"}><strong> Caregiver Management </strong></span>
                </CardHeader>
                <Card className={"table-background"}>
                    <div className={"breaker"}></div>
                    <Row className={"table-row"}>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button className={"loginButton"} onClick={this.toggleForm}>Add Caregiver </Button>
                        </Col>
                    </Row>
                    <div className={"breaker"}></div>
                    <Row className={"table-row"}>
                        <Col sm={{size: '12', offset: 0}}>
                            {this.state.isLoaded && <CaregiverTable tableData = {this.state.tableData} reloadHandler1={this.reload1} params={this.handleId}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    {this.state.updateForm === false && this.state.showPatients === false  && <ModalHeader toggle={this.toggleForm}> Add Caregiver: </ModalHeader>}
                    {this.state.updateForm === true && this.state.showPatients === false && <ModalHeader toggle={this.toggleForm}> Edit Caregiver: </ModalHeader> }
                    {this.state.showPatients === true && <ModalHeader> Patients: </ModalHeader>}


                    <ModalBody>
                        { this.state.updateForm === false && this.state.showPatients === false && <CaregiverForm reloadHandler={this.reload}/>}
                        {this.state.updateForm === true && this.state.showPatients === false && <CaregiverUpdateForm reloadHandler={this.reload} caregiver={this.caregiver}/>}
                        {this.state.showPatients === true && <PatientTable patientData = {patientData} viewOnly={1} cols={0}/>}
                        {this.state.updateForm  = false}
                        {this.state.showPatients = false}

                    </ModalBody>
                </Modal>

            </div>
        )

        else return (<Redirect to={{
            pathname: '/login'

        }} />)

    }
}


export default CaregiverContainer;
