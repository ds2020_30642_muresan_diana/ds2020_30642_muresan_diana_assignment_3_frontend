import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col, FormGroup, Input, Label,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "../caregiver/api/caregiver-api"
import CaregiverTable from "../caregiver/components/caregiver-table";
import PatientTable from "../patient/components/patient-table";
import * as API_USERS_2 from "../patient/api/patient-api"
import {Redirect} from "react-router-dom";

import ReactNotification from 'react-notifications-component'

import { store } from  "react-notifications-component"
import 'react-notifications-component/dist/theme.css'
import Stomp from 'stompjs'

import SockJsClient from 'react-stomp';


class Caregiver extends React.Component {

    constructor(props) {
        super(props);

        this.stompClient=null;
        this.dataSent = props.location;
        //this.username = props.location.state.username;
       // console.log(props);
        //console.log(props.location.state.username);
        this.reload = this.reload.bind(this);
        this.handleLogout = this.handleLogout.bind(this);



        this.caregiver = {
            id: null,
            name: null,
            birthdate: null,
            gender: null,
            address: null,
            username: null,
            password: null
        }
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            logout: false,
            existsPatient:false


        };
    }

    componentDidMount() {
        if (localStorage.getItem('user') === "caregiver") {
            this.fetchCaregiver(this.dataSent.state.username);
            this.fetchPatientsByCaregiver(this.dataSent.state.username);
        }

    }

    fetchPatientsByCaregiver(username) {
        return API_USERS_2.getPatientsByCaregiver(username,(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    reload() {
        this.setState({
            isLoaded: false
        });

        this.fetchPatients();
    }

    handleLogout() {
        this.setState({
            logout: true
        })
    }
    fetchCaregiver(username) {
        return API_USERS.getCaregiverByUsername(username,(result, status, err) => {

            if (result !== null && status === 200) {
                this.caregiver.id = result.id;
                this.caregiver.name = result.name;
                this.caregiver.address = result.address;
                this.caregiver.gender = result.gender;
                this.caregiver.birthdate = result.birthdate;
                this.caregiver.username = result.username;
                this.caregiver.password = result.password;
                console.log(this.caregiver);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

     handleNotification(message, name) {
        let mess = '';
        if (message === 'Sleeping') {
            mess = name + " slept too much";
        }
        if (message === 'Leaving') {
            mess = name + " was gone for too long";
        }
        if (message === 'Showering' || message === 'Toileting' || message === 'Grooming') {
            mess =  name + " spent too much time in the bathroom";
        }

        store.addNotification({
            title:"Warning!",
            message: mess,
            type: "warning",
            container: "top-right",
            insert: "top",
            animationIn: ["animated", "zoomInLeft"],
            animationOut: ["animated", "zoomOutLeft"],
            width: 500,
            dismiss: {
                duration: 10000,
                showIcon: true,
                onScreen: true,
                pauseOnHover: true
            }

        })
    }


    render() {


        const { logout } = this.state;
        if ( logout === true ) {
            localStorage.removeItem('user');
            return (<Redirect to={{
                pathname: '/login'

            }} />)
        }
        if (localStorage.getItem('user') === "caregiver")
         return (

            <div>
                <SockJsClient url='https://diana-muresan-medical-backend.herokuapp.com/activity-websocket'
                              topics={['/topic/activities']}

                              onMessage={(msg) => {console.log(msg);
                                  for (let i = 0; i<this.state.tableData.length; i++){
                                      if (this.state.tableData[i].id === msg.patientId)
                                          this.handleNotification(msg.activity, this.state.tableData[i].name);
                                  }
                                 }}
                        />

                <CardHeader>
                    <span className={"info-title"}><strong> Your Patients </strong></span>
                    <button className={"logout-button"} onClick={this.handleLogout}> Logout</button>
                </CardHeader>
                <ReactNotification/>


                <Card className={"table-background2"}>

                    <Row className={"table-row"}>
                        <Col sm={{size: '12', offset: 0}} >
                            {this.state.isLoaded && <PatientTable tableData = {this.state.tableData} reloadHandler={this.reload} viewOnly={0} cols={0}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>

                </Card>

            </div>

        )

        else return (<Redirect to={{
            pathname: '/login'

        }} />)

    }
}


export default Caregiver;
